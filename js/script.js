$(function () {
    $('#menu__link').popmenu({ 'width': '622px', 'box-shadow': '0px 5px 10px rgba(47, 48, 58, 0.1)', 'height': '396px', 'background': '#fff', 'borderRadius': '0' });
	
	$('.product__left').slick({
	dots: true
	});
				
})


$(window).scroll(function () {
	if ($(window).width() > 1024) {

		$('.menu__wrapper').each(function () {

			var elemPosTop = $(this).offset().top;
			var topOfWindow = $(window).scrollTop();

			var top = elemPosTop;
			var bottom =elemPosTop + $(this).height();
			if (top < topOfWindow) {

				$('#mMenu').addClass('fixed');
				$('#mMenuPlaceholer').show();
			}
			else {
				$('#mMenu').removeClass('fixed');
				$('#mMenuPlaceholer').hide();
			}
		});
	}
});
